module product

go 1.16

replace product/router => ./router

require (
	github.com/gin-gonic/gin v1.7.2
	gitlab.com/golang-lab/shop-proto v0.0.0-20210627071136-a8053f4731cf
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	google.golang.org/genproto v0.0.0-20210629200056-84d6f6074151 // indirect
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1 // indirect
	product/router v0.0.0-00010101000000-000000000000
)
