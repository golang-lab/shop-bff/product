package router

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/golang-lab/shop-proto/proto/product"
)

// default route, return success connection
func Endpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "product server is running",
	})
}

// get products
func ProductGet(c *gin.Context, client pb.ProductServiceClient) error {

	bytes, err := io.ReadAll(c.Request.Body)
	if err != nil {
		return err
	}
	defer c.Request.Body.Close()

	var body *pb.ProductRequest
	err = json.Unmarshal(bytes, &body)
	if err != nil {
		return err
	}
	log.Println(&body)

	var res *pb.ProductResponse
	res, err = client.Get(c, body)
	if err != nil {
		return err
	}

	c.JSON(http.StatusOK, gin.H{
		"result": res,
	})

	return nil
}
