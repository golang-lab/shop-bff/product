package main

import (
	"log"

	router "product/router"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/golang-lab/shop-proto/proto/product"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := pb.NewProductServiceClient(conn)

	// Set up a http server.
	r := gin.Default()

	r.GET("/", router.Endpoint)

	r.POST("/product/get", func(c *gin.Context) {
		router.ProductGet(c, client)
	})

	// Run http server
	if err := r.Run(":8081"); err != nil {
		log.Fatalf("could not run server: %v", err)
	}
}
